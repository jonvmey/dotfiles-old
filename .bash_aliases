# .bash_aliases
# Author: Jonathan Vander Mey <jonathan@vandermey.ca>
# Source: https://gitlab.com/grimmn/dotfiles.git

# Bash aliases sourced by bashrc

# ls options
export LS_OPTIONS="--color=auto"
eval "`dircolors`"

# ls aliases
alias ls="ls $LS_OPTIONS"
alias ll="ls $LS_OPTIONS -lh"
alias la="ls $LS_OPTIONS -a"
alias lla="ls $LS_OPTIONS -lha"
alias l='ls $LS_OPTIONS -lhAtr'

# Directory aliases
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias up="cd ..;ls $LSOPTIONS"
alias p="pwd"
alias dirs="dirs -v"
alias u="pushd .."
alias d="popd"

# File aliases
alias rm='rm -I --preserve-root'
alias cp="cp -i"
alias mv="mv -i"
alias ln="ln -i"

# Quick edit aliases
alias ea="vim $HOME/.aliases"
alias eb="vim $HOME/.bashrc"
alias eba="vim $HOME/.bash_aliases"
alias ebp="vim $HOME/.bash_profile"
alias egc="vim $HOME/.gitconfig"
alias et="vim $HOME/.tmux.conf"
alias ev="vim $HOME/.vimrc"

# Permissions aliases
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'

# Misc modified commands
alias sudo='sudo ' # Let aliases be run with sudo
alias vi='vim'
alias vim='nvim'
alias cpProgress='rsync --progress -ravz'
alias mkdir='mkdir -pv'
alias wget='wget -c'
alias grep='grep --color=auto --exclude-dir=.svn --exclude=tags'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias svncl='svn cleanup --remove-unversioned'
alias ctags='ctags --exclude=.svn --exclude=svn_externals/rootfs'
alias sbr='source ~/.bashrc'
alias minicom='minicom -w'
alias updates='sudo apt-get update && sudo apt-get dist-upgrade'
alias p='sudo pacman'
alias S='sudo systemctl'
alias launch-dropdown='alacritty --class Alacritty,dropdown --command tmux at -t dropdown &'
alias clear-swap='sudo swapoff -a && sudo swapon -a'
alias nvim-plug='nvim +PlugInstall +PlugClean +PlugUpdate +UpdateRemotePlugins'
alias set-bg='feh --bg-scale'
alias ip='ip -c'
alias chmox='chmod +x'

function cs(){
  cd $1
  ls
}
function gitdiff(){
  git diff $1 > git.diff
  vim git.diff
}
function gitlog(){
  git log $1 > git.log
  vim git.log
}
function svndiff(){
  if [[ -z $1 ]]; then
    svn st -q | grep '^ \{0,1\}[ADMR]' | cut -c9- | xargs svn diff > svn.diff
  else
    svn diff $1 > svn.diff
  fi
  vim svn.diff
}
function cvsdiff(){
  cvs diff $1 > cvs.diff
  vim cvs.diff
}
function svnlog(){
  svn log $1 > svn.log
  vim svn.log
}
function cvslog(){
  cvs log $1 > cvs.log
  vim cvs.log
}
