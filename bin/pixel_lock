#!/usr/bin/bash

img_file=/tmp/lockscreen.png

# Determine dimensions of all combined screens
# Making an assumption that all screens are positioned in one horizontal row
# Use the maximum screen height as the total height if any of them are
# different and add all widths into one total width
screen_geometries=$(xrandr | grep " connected" | grep -o "[0-9]\+x[0-9]\++[0-9]\++[0-9]\+")
screen_widths=$(echo "$screen_geometries" | cut -d '+' -f 1 | cut -d 'x' -f 1)
screen_heights=$(echo "$screen_geometries" | cut -d '+' -f 1 | cut -d 'x' -f 2)

if [[ -z "$screen_widths" || -z "$screen_heights" ]]; then
  echo "Couldn't get screen dimensions from XRandR, exiting."
  exit
fi

screen_width_array=( "$screen_widths" )
screen_height_array=( "$screen_heights" )
total_screen_width=0
total_screen_height=${screen_height_array[0]}

for width in "${screen_width_array[@]}"; do
  total_screen_width=$(( total_screen_width + width ))
done

for height in "${screen_height_array[@]}"; do
  if [[ $height -gt $total_screen_height ]]; then
    total_screen_height=$height
  fi
done

scaled_screen_width=$((total_screen_width / 20))
scaled_screen_height=$((total_screen_height / 20))

# Capture background
import -window root $img_file

# Downscale
convert $img_file -resize "$scaled_screen_width"x"$scaled_screen_height" $img_file

# Upscale
convert $img_file -sample "$total_screen_width"x"$total_screen_height" $img_file

# Temporarily suspend desktop notifications
killall -SIGUSR1 dunst

# Lock screen with generated image as background, do not fork to ensure that
# notifications stay suspended
i3lock -n -i $img_file

rm -f $img_file

# Resume notifications
killall -SIGUSR2 dunst
