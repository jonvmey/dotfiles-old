#!/bin/sh

# Login profile

export PATH="$PATH:$HOME/bin"
export EDITOR="nvim"
export TERMINAL="alacritty"
export BROWSER="firefox"

# SSH Agent
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

# shellcheck source=/dev/null
[ -f ~/.bashrc ] && . "$HOME/.bashrc"
