" .vimrc
" Author: Jonathan Vander Mey <jonathan@vandermey.ca>
" Source: https://gitlab.com/grimmn/dotfiles.git
"
" My vimrc file, a large portion of which has been gleaned from Steve
" Losh's, available at https://bitbucket.org/sjl/dotfiles/src/tip/vim

" Preamble ---------------------------------------------------------------- {{{

" Automatically download vim-plug file if not present
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Plugin List for vim-plug
call plug#begin()
Plug 'vim-scripts/a.vim'
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'jiangmiao/auto-pairs'
Plug 'itchyny/calendar.vim'
Plug 'tpope/vim-commentary'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'will133/vim-dirdiff'
Plug 'Konfekt/FastFold'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-git'
Plug 'airblade/vim-gitgutter'
Plug 'vim-scripts/matchit.zip'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tpope/vim-surround'
Plug 'godlygeek/tabular'
Plug 'majutsushi/tagbar'
Plug 'SirVer/ultisnips'
Plug 'tpope/vim-unimpaired'
Plug 'vim-scripts/vcscommand.vim'
Plug 'idanarye/vim-vebugger'
Plug 'Shougo/vimproc.vim', {'do' : 'make'}
Plug 'vimwiki/vimwiki'
Plug 'w0rp/ale'
if has('nvim')
  Plug 'Shougo/deoplete.nvim', {'do' : ':UpdateRemotePlugins'}
  Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
Plug 'zchee/deoplete-clang'
Plug 'zchee/deoplete-jedi'
Plug 'Shougo/neoinclude.vim'
Plug 'Shougo/echodoc.vim'
call plug#end()

" }}} Preamble
" Basic Settings ---------------------------------------------------------- {{{
set showcmd
set noshowmode
set visualbell
set ttyfast
set ruler
set cursorline
set encoding=utf-8
set backspace=indent,eol,start
set list
set listchars=tab:▸\ ,eol:¬,extends:>,precedes:<
set fillchars=diff:⣿
set number
set relativenumber
set splitbelow
set splitright
set lazyredraw
set autoread
set autowrite
set shiftround
set undofile
set undoreload=10000
set title
set linebreak
set showbreak=..
set breakindent
set history=1000
set nrformats-=octal
set hidden
set laststatus=2
set tags=tags;/
highlight colorcolumn ctermbg=0*

" Time out on key codes but not mappings.
" Basically this makes terminal vim work sanely.
set notimeout
set ttimeout
set ttimeoutlen=100

" Make vim able to edit crontab files again.
set backupskip=/tmp/*,/private/tmp/*"

" Better Completion
set complete=.,w,b,u,t
set completeopt=longest,menuone,preview

" Save when losing focus
au FocusLost * : silent! wall

" Leader
let mapleader=","
let maplocalleader="\\"

" Better status line:
" set statusline=%F%m%r%h%w\ (%{&ff}){%Y}\ [%l,%v][%p%%]

" Cursorline {{{
" Only show cursorline in the active window and in normal mode

augroup cline
  au!
  au WinLeave,InsertEnter * set nocursorline
  au WinEnter,InsertLeave * set cursorline
augroup END

" }}} Cursorline
" Trailing whitespace {{{
"
" Only show when not in insert mode
augroup trailing
  au!
  au InsertEnter * :set listchars-=trail:⌴
  au InsertLeave * :set listchars+=trail:⌴
augroup END

" Highlight trailing whitepace and spaces before a tab
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$\| \+\ze\t/
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red

" }}} Trailing whitespace
" Wildmenu completion {{{

set wildmenu
set wildignore+=.git,.svn                            " Version control
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg       " Binary images
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest,*.P " Compiled object files
set wildignore+=*.gcno,*.gcda                        " gcc coverage files
set wildignore+=*.sw?                                " Vim swap files
set wildignore+=*.pyc                                " Python byte code
set wildignore+=*.orig                               " Merge resolution files
set wildignore+=*.P                                  " Make dependencies"

" }}} Wildmenu completion
" Line Return {{{
" Make sure Vim returns to the same line when reopening a file.

augroup line_return
  au!
  au BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   execute 'normal! g`"zvzz' |
    \ endif
augroup END

" }}} Line Return
" Tabs, spaces, wrapping {{{

set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set smarttab
set wrap
set textwidth=80
set colorcolumn=+1
set formatoptions-=t

" }}} Tabs, spaces, wrapping
" Backups {{{

set backup
set noswapfile

set undodir=~/.vim/tmp/undo//       " undo files
set backupdir=~/.vim/tmp/backup//   " backup files
set directory=~/.vim/tmp/swap//     " swap files

" Make those directories automatically if they don't already exist
if !isdirectory(expand(&undodir))
  call mkdir(expand(&undodir), "p")
endif
if !isdirectory(expand(&backupdir))
  call mkdir(expand(&backupdir), "p")
endif
if !isdirectory(expand(&directory))
  call mkdir(expand(&directory), "p")
endif

" }}} Backups
" Colorscheme {{{

syntax on
set background=dark
colorscheme molokai
" colorscheme afterglow

" Highlight VCS conflict markers
match ErrorMsg '^\(<\|=\|>\)\{7\}\([^=].\+\)\?$'

" Transparent background
hi Normal  guibg=NONE ctermbg=NONE
hi NonText guibg=NONE ctermbg=NONE
hi Folded  guibg=NONE ctermbg=NONE

" }}} Colorscheme

" }}} Basic Settings
" Abbreviations ----------------------------------------------------------- {{{

function! EatChar(pat)
  let c = nr2char(getchar(0))
  return (c =~ a:pat) ? '' : c
endfunction

function! MakeSpacelessIabbrev(from, to)
  execute "iabbrev <silent> ".a:from." ".a:to."<C-R>=EatChar('\\s')<CR>"
endfunction
function! MakeSpacelessBufferIabbrev(from, to)
  execute "iabbrev <silent> <buffer> ".a:from." ".a:to."<C-R>=EatChar('\\s')<CR>"
endfunction

call MakeSpacelessIabbrev('va/',    'http://vandermey.ca/')
call MakeSpacelessIabbrev('gh/',    'http://github.com/')
call MakeSpacelessIabbrev('ghg/',   'http://github.com/grimmn/')

iabbrev jv@ jonathan@vandermey.ca

" Typo fixers
iabbrev todo TODO
iabbrev flase false
iabbrev incldue include
iabbrev widht width
iabbrev uin8_t uint8_t
iabbrev uin16_t uint16_t
iabbrev uin32_t uint32_t

" }}} Abbreviations
" Convenience mappings ---------------------------------------------------- {{{

" Disable help key
noremap  <F1> :checktime<CR>
inoremap <F1> <esc>:checktime<CR>

" Kill window
nnoremap K :q<CR>

" Save
nnoremap s :w<CR>

nnoremap M K

" Toggle line numbers
nnoremap <leader>n :setlocal number!<CR>

" Sort lines
nnoremap <leader>s vip:!sort<CR>
vnoremap <leader>s :!sort<CR>

" Tabs
nnoremap <leader>( :tabprev<CR>
nnoremap <leader>) :tabnext<CR>

" Wrap toggle
nnoremap <leader>W :set wrap!<CR>

" Inserting blank lines with Enter
nnoremap <CR> o<esc>
" Disable Enter mapping inside of quickfix windows
autocmd BufReadPost quickfix nnoremap <buffer> <CR><CR>

" Copying/pasting text to/from system clipboard.
noremap <leader>p "+p
vnoremap <leader>y "+y

" Yank to end of line
nnoremap Y y$

" Reselect last-pasted text
nnoremap gv `[v`]

" Linewise selection of last-pasted text
nnoremap <leader>V V`]

" Clean trailing whitespace
nnoremap <leader>wf mz:%s/\s\+$//<CR>:let @/=''<CR>`z

" Select entire buffer
nnoremap vaa ggvGg_
nnoremap Vaa ggVG

" Uppercase word
inoremap <C-y> <esc>mzgUiw`za

" Panic Button
nnoremap <f9> mzggg?G`z

" Diffoff
nnoremap <leader>D :diffoff!<CR>

" Split line (sister to [J]oin line)
" Normal use of S is covered by cc
nnoremap S i<CR><esc>^mzgk:silent! s/\v +$//<CR>:noh<CR>`z

" Substitute
nnoremap <C-s> :%s/
vnoremap <C-s> :s/

" Re-source vimrc
nnoremap <leader>sv :source $MYVIMRC<CR>

" Source line
vnoremap <leader>S y:@"<CR>
nnoremap <leader>S ^vg_y:execute @@<CR>:echo 'Sourced line.'<CR>

" Marks and Quotes
noremap ' `
noremap ` <C-^>

" Select (charwise) the contents of the current line excluding indentation
nnoremap vv ^vg_

" Toggle paste mode
nnoremap <F6> :set paste!<CR>

" Toggle [i]nvisible characters
nnoremap <leader>i :set list!<CR>

" Unfuck the screen
nnoremap U :syntax sync fromstart<CR>:redraw!<CR>

" Make Q more useful, never used Ex mode yet
nnoremap Q gqip

" Zip Right
" Moves the character under the cursor to the end of the line.
" Handy for fixing surrounding characters.
nnoremap zl :let @z=@"<CR>x$p:let @"=@z<CR>

" Easier : commands
nnoremap ; :

" Easier escape from insert mode
inoremap jk <esc>
inoremap Jk <esc>
inoremap JK <esc>

" Insert Mode Completion {{{

" Completion of file, tag and lines in insert mode
inoremap <C-f> <C-x><C-f>
inoremap <C-]> <C-x><C-]>
inoremap <C-l> <C-x><C-l>

" }}} Insert Mode Completion

"Window Resizing

" right/up  : bigger
" left/down : smaller
nnoremap <C-right> :vertical resize +3<CR>
nnoremap <C-left> :vertical resize -3<CR>
nnoremap <C-up> :resize +3<CR>
nnoremap <C-down> :resize -3<CR>

" Insert current date
nnoremap <leader>da :put!=strftime('%a %b %d %Y')<CR>j

" Insert current date and time
nnoremap <leader>ti :put!=strftime('%c')<CR>j

" Append modeline after last line in buffer.
" Use substitute() instead of printf() to handle '%%s' modeline in LaTeX
" files.
function! AppendModeline()
  let l:modeline = printf(" vim: set ts=%d sw=%d tw=%d %set :",
        \ &tabstop, &shiftwidth, &textwidth, &expandtab ? '' : 'no')
  let l:modeline = substitute(&commentstring, "%s", l:modeline, "")
  call append(line("$"), l:modeline)
endfunction
nnoremap <silent> <Leader>ml :call AppendModeline()<CR>

" }}} Convenience mappings
" Quick editing ----------------------------------------------------------- {{{

nnoremap <leader>ev :vsplit ~/.vimrc<CR>
nnoremap <leader>eb :vsplit ~/.bashrc<CR>
nnoremap <leader>ea :vsplit ~/.bash_aliases<CR>
nnoremap <leader>eg :vsplit ~/.gitconfig<CR>
nnoremap <leader>et :vsplit ~/.tmux.conf<CR>
nnoremap <leader>ei :vsplit ~/.config/i3.config<CR>

" }}} Quick editing
" Searching and movement -------------------------------------------------- {{{

" Use sane regexes
nnoremap / /\v
vnoremap / /\v

set ignorecase
set smartcase
set incsearch
set showmatch
set matchtime=3
set hlsearch
set gdefault

set scrolloff=3
set sidescroll=1
set sidescrolloff=10

set virtualedit+=block

noremap <silent> <leader><space> :noh<CR>call clearmatches()<CR>

" Use tab for
map <tab> %

" D deletes to end of line
nnoremap D d$

" Don't move cursor when searching with *
nnoremap <silent> * :let stay_star_view = winsaveview()<CR>*:call winrestview(stay_star_view)<CR>

" Jumping to tags
" Set <C-]> to jump to tags like normal and <C-\> to open the tag in a new
" split instead.
" Both will align the destination line to the upper middle part of the
" screen, pulse the new cursor position and <C-\> will fold the buffer
" and unfold enough to see the destination line.
function! JumpToTag()
  execute "normal! \<C-]>mzzvzz15\<C-e>"
  execute "keepjumps normal! `z"
  Pulse
endfunction
function! JumpToTagInSplit()
  execute "normal! \<C-w>v\<C-]>mzzMzvzz15\<C-e>"
  execute "keepjumps normal! `z"
  Pulse
endfunction
nnoremap <C-]> :silent! call JumpToTag()<CR>
nnoremap <C-\> :silent! call JumpToTagInSplit()<CR>

" Keep search matches in the middle of the window
nnoremap n nzzzv
nnoremap N Nzzzv

" Same when jumping around
nnoremap g; g;z
nnoremap g, g,zz
nnoremap <C-o> <C-o>zz

" Easier to type
noremap H ^
noremap L $
vnoremap L g_

" Heresy
inoremap <C-a> <esc>I
inoremap <C-e> <esc>A
cnoremap <C-a> <home>
cnoremap <C-e> <end>

" gi already moves to "last place you exited insert mode", so map gI to
" something similar: move to last change
nnoremap gI `.

" Fix linewise visual selection of various text objects
nnoremap VV V
nnoremap Vit vitVkoj
nnoremap Vat vatV
nnoremap Vab vabV
nnoremap VaB vaBV

" Direction Keys {{{

noremap j gj
noremap k gk
noremap gj j
noremap gk k

" Easy buffer navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

noremap <leader>v <C-w>v
noremap <leader>h <C-w>s

" }}} Direction Keys
" Visual Mode */# from Scrooloose {{{

function! s:VSetSearch()
  let temp = @@
  norm! gvy
  let @/ = '\V' . substitute(escape(@@, '\'), '\n', '\\n', 'g')
  let @@ = temp
endfunction

vnoremap * :<C-u>call <SID>VSetSearch()<CR>//<CR><C-o>
vnoremap # :<C-u>call <SID>VSetSearch()<CR>??<CR><C-o>

" }}} Visual Mode */#
" List Navigation {{{

nnoremap <left>  :cprev<CR>zvzz
nnoremap <right> :cnext<CR>zvzz
nnoremap <up>    :lprev<CR>zvzz
nnoremap <down>  :lnext<CR>zvzz

" }}} List Navigation

" }}} Searching and movement
" Folding ----------------------------------------------------------------- {{{

set foldlevelstart=0

" Space to toggle folds
nnoremap <Space> za
vnoremap <Space> za

" "Focus" the current line by closing all folds, open just the folds
" containing the current line, move the line to just about the center of
" the screen and pulse the cursor line.
" This mapping wipes out the z mark.
" Can use :sus for instances where Vim should be backgrounded.
nnoremap <C-z> mzzMzvzz15<C-e>`z:Pulse<CR>

function! MyFoldText() " {{{
  let line = getline(v:foldstart)

  let nocolwidth = &fdc + &number * &numberwidth
  let windowwidth = winwidth(0) - nocolwidth - 3
  let foldedlinecount = v:foldend - v:foldstart

  " expand tabs into spaces
  let onetab = strpart('          ', 0, &tabstop)
  let line = substitute(line, '\t', onetab, 'g')

  let line = strpart(line, 0, windowwidth - 2 - len(foldedlinecount))
  let fillcharcount = windowwidth - len(line) - len(foldedlinecount)
  return line . '…' . repeat(" ",fillcharcount) . foldedlinecount . '…' . ' '
endfunction " }}}
set foldtext=MyFoldText()

" }}} Folding
" Filetype-specific ------------------------------------------------------- {{{

" Skeleton Templates {{{

augroup templates
  au!
  " read in template file
  autocmd BufNewFile *.* silent! execute '0r ~/.vim/templates/skeleton.'.expand("<afile>:e")
augroup END

" }}} Skeleton Templates

" Assembly {{{

augroup ft_asm
  au!
  au FileType asm setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2
augroup END

" }}} Assembly
" C {{{

augroup ft_c
  au!
  " au FileType c setlocal foldmethod=marker foldmarker={,}
  au FileType c setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2
  au FileType c setlocal cino=(0
  " ) This line is to fix format highlighting only
augroup END

" }}} C
" C++ {{{

augroup ft_cpp
  au!
  " au FileType c setlocal foldmethod=marker foldmarker={,}
  au FileType cpp setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2
  au FileType cpp setlocal commentstring=//%s
  au FileType cpp setlocal cino=(0
  ") This line is to fix format highlighting only
augroup END

" }}} C++
" Diff {{{

" This is from https://github.com/sgeb/vim-diff/fold
function! DiffFoldLevel()
  let l:line=getline(v:lnum)

  if l:line =~# '^\(diff\|Index\)'        " file
  return '>1'
  elseif l:line =~# '^\(@@\|\d\)'         " hunk
  return  '>2'
  elseif l:line =~# '^\*\*\* \d+,\d\+ \*\*\*\*$' " context: file1
  return '>2'
  else
  return '='
  endif
endfunction

" Automatically fold diff files
augroup ft_diff
  au!

  autocmd FileType diff setlocal foldmethod=expr
  autocmd FileType diff setlocal foldexpr=DiffFoldLevel()
augroup END

autocmd BufRead cvs.diff set foldexpr=getline(v:lnum+1)=~'^Index:.*$'?'<1':1
autocmd BufRead svn.diff set foldexpr=getline(v:lnum+1)=~'^Index:.*$'?'<1':1
autocmd BufRead hg.diff set foldexpr=getline(v:lnum+1)=~'^diff.*$'?'<1':1

" }}} Diff
" Makefile {{{

augroup ft_make
  au!

  au FileType make setlocal noexpandtab shiftwidth=4 tabstop=4 softtabstop=4
augroup END
" }}} Make
" Python {{{

" }}} Python
" SDC {{{

autocmd BufNewFile,BufRead *.sdc set filetype=tcl

" }}} SDC
" Tmux {{{

augroup ft_tmux
  au!

  au FileType tmux setlocal foldmethod=marker
augroup END

" }}}
" VHDL {{{

" Add VHDL constructs to matchit definitions taken from:
" vim_use.narkive.com/zzFd6jbH/matching-begin-end-pairs
function! VHDLMatchIt()
  if ! exists("b:match_words") && exists("loaded_matchit")
    let b:match_ignorecase=1
    let s:notend = '\%(\<end\s\+\)\@<!'
    let b:match_words=
          \ s:notend . '\<if\>:\<elsif\>:\<else\>:\<end\>\s\+\if\>,' .
          \ s:notend . '\<case\>:\<when\>:\<end\>\s\+\<case\>,' .
          \ s:notend . '\<process\>:\<end\>\s\+\<process\>'
    " TODO: Add in other pairs here
  endif
endfunction

augroup ft_vhdl
  au!

  au FileType vhdl setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2
  autocmd FileType vhdl setlocal commentstring=--\ %s
  autocmd FileType vhdl call VHDLMatchIt()
augroup END

let g:tagbar_type_vhdl = {
    \ 'ctagsbin': 'vhdl-tool',
    \ 'ctagsargs': 'ctags -o -',
    \ 'ctagstype': 'vhdl',
    \ 'kinds' : [
        \'d:prototypes',
        \'b:package bodies',
        \'e:entities',
        \'a:architectures',
        \'t:types',
        \'p:processes',
        \'f:functions',
        \'r:procedures',
        \'c:constants',
        \'T:subtypes',
        \'r:records',
        \'C:components',
        \'P:packages',
        \'l:locals',
        \'i:instantiations',
        \'s:signals',
        \'v:variables:1:0'
    \ ],
    \ 'sro' : '::',
    \ 'kind2scope' : {
         \ 'a' : 'architecture',
         \ 'b' : 'packagebody',
         \ 'P' : 'package',
         \ 'p' : 'process'
    \ },
    \ 'scope2kinds' : {
         \ 'architecture' : 'a',
         \ 'packagebody'  : 'b',
         \ 'package'      : 'P',
         \ 'process'      : 'p'
    \ }
\}

" }}} VHDL
" Vim {{{

augroup ft_vim
  au!

  au FileType vim setlocal foldmethod=marker keywordprg=:help
  au FileType help setlocal textwidth=78
  au BufWinEnter *.txt if &ft == 'help' | wincmd L | endif
augroup END

" }}} Vim

" }}} Filetype-specific
" Plugins ----------------------------------------------------------------- {{{

" A (alternate) {{{

nnoremap <leader>a :A<CR>

" }}} A (alternate)
" ALE {{{

let g:ale_linters = {'c': ['clangtidy', 'cppcheck', 'cquery'],
                   \ 'cpp': ['clangtidy', 'cppcheck', 'cquery'],
                   \ 'rust': ['rls'],
                   \ 'sh': ['shellcheck']}

let g:ale_c_parse_compile_commands = 1

let g:ale_cpp_clangtidy_checks = ['*', '-llvm-include-order', '-llvm-header-guard']
let g:ale_cpp_cppcheck_options = '--enable=warning,style,performance,portability,information,missingInclude'

" }}} ALE
" Airline {{{

let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

" }}} Airline "
" Calendar {{{

let g:calendar_google_calendar=1
let g:calendar_google_task=1

" }}} Calendar
" Ctrl-P {{{

let g:ctrlp_working_path_mode = 'a'

" }}} Ctrl-P
" Deoplete {{{

let g:deoplete#enable_at_startup = 1
" Automatically close preview window after completion finishes
autocmd CompleteDone * pclose!

" Use tab-completion
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"

" }}} Deoplete
" Deoplete-Clang {{{

let g:deoplete#sources#clang#libclang_path = "/usr/local/lib/libclang.so"
let g:deoplete#sources#clang#clang_header = "/usr/local/lib/clang/6.0.1"

" }}} Deoplete-Clang
" DirDiff {{{

" Taken from Jayesh's vimrc (I think)
let g:DirDiffExcludes = "xst,CVS,*.exe,*.elf,.*.sw*,*.vho,*.ngo,_ngo,*.o,_.so,*.a,*.dep,.svn,.hg,*.diff,*.xmsgs,*.gz,*.tgz,*.hdb,*.cdb,*.rdb"
let g:DirDiffIgnore = "Id:,Revision:,Date:,Author:"
autocmd Bufread *.vhd let g:DirDiffIgnoreCase = 1
let g:DirDiffDynamicDiffText = 1

" }}} DirDiff
" EchoDoc {{{

let g:echodoc#enable_at_startup = 1
let g:echodoc#type = 'signature'

" }}} EchoDoc
" Fugitive {{{

nnoremap <leader>gd :Gvdiffsplit<CR>
nnoremap <leader>gs :Git<CR>
nnoremap <leader>gw :Gwrite<CR>
nnoremap <leader>gb :Git_blame<CR>
nnoremap <leader>gci :Git commit<CR>
nnoremap <leader>gm :GMove<CR>
nnoremap <leader>gr :GRemove<CR>
nnoremap <leader>gP :Git push<CR>
nnoremap <leader>gl :Shell git gl -18<CR>:wincomd \|<CR>

augroup ft_fugitive
  au!

  au BufNewFile,BufRead .git/index setlocal nolist
augroup END

" }}} Fugitive
" LanguageClient {{{

let g:LanguageClient_serverCommands = {
    \ 'c'  : ['cquery', '--log-file=/tmp/cq.log', '--init={"cacheDirectory":/var/cquery"}'],
    \ 'cpp'  : ['cquery', '--log-file=/tmp/cq.log', '--init={"cacheDirectory":/var/cquery"}'],
    \ 'rust'  : ['rustup', 'run', 'stable', 'rls'],
    \ }

let g:LanguageClient_autoStart = 1

function! SetLSPShortcuts()
  nnoremap <silent> M :call LanguageClient_textDocument_hover()<CR>
  nnoremap <leader>lh :call LanguageClient_textDocument_hover()<CR>
  nnoremap <silent> gd :call LanguageClient_textDocument_definition()<CR>
  nnoremap <leader>ld :call LanguageClient_textDocument_definition()<CR>
  nnoremap <leader>lr :call LanguageClient_textDocument_rename()<CR>
  nnoremap <leader>lsd :call LanguageClient_textDocument_documentSymbol()<CR>
  nnoremap <leader>lx :call LanguageClient_textDocument_references()<CR>
  nnoremap <leader>lsw :call LanguageClient_workspace_symbol()<CR>
  nnoremap <leader>lf :call LanguageClient_textDocument_formatting()<CR>

  " Allows usage of gq to format selected lines
  set formatexpr=LanguageClient_textDocument_rangeFormatting()
endfunction()

augroup LSP
  autocmd!
  " Override certain bindings for filetypes with LSP support
  autocmd FileType c,cpp,rust call SetLSPShortcuts()
augroup END


" }}} LanguageClient
" NERDTree {{{

" Open NERDTree automatically whenever Vim is opened.
" autocmd vimenter * NERDTree

" Open NERDTree automatically when Vim starts with no files specified.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Mapping to open NERDTree
nnoremap <leader>nt :NERDTreeToggle<CR>

" Close Vim when NERDTree is the only buffer left open.
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Show Bookmarks in NERDTree
let g:NERDTreeShowBooksmarks=1

" Hide files with certain extensions in NERDTree
let g:NERDTreeIgnore=['\.o', '\.a', '\.gcno', '\.gcda']

" }}} NERDTree
" TagBar {{{

nnoremap <leader>tb :Tagbar<CR>

" }}} TagBar
" UltiSnips {{{

" Remap expand trigger to eliminate YCM conflict on tab
let g:UltiSnipsExpandTrigger="<C-j>"
let g:UltiSnipsJumpForwardTrigger="<C-j>"
let g:UltiSnipsJumpBackwardTrigger="<C-k>"
let g:UltiSnipsEditSplit="vertical"

" Explicitly define snippets directory for faster operation
let g:UltiSnipsSnippetDirectories=[$HOME.'/.vim/UltiSnips']

" SnipMate compatible author variable
if !exists('g:snips_author')
  let g:snips_author="Jonathan Vander Mey"
endif

" }}} UltiSnips
" VimWiki {{{

" Prevent vimwiki from hijacking the .md extension outside of wiki directories
let g:vimwiki_global_ext=0

" Wiki Index List
let g:vimwiki_list=[{'path': expand('~/wiki/'),
                   \ 'syntax': 'markdown', 'ext': '.md'}]

" Enable wiki list folding
" let g:vimwiki_folding='expr'

" Enable Calendar.vim in vimwiki
let g:vimwiki_use_calendar=1

" }}} VimWiki

" }}} Plugins
" Text Objects ------------------------------------------------------------ {{{

" Folds {{{

onoremap if :<C-u>normal! [zv]z<CR>
onoremap af :<C-u>normal! [zV]z<CR>
vnoremap if :<C-u>normal! ]zv[z<CR>
vnoremap af :<C-u>normal! ]zV[z<CR>

" }}} Folds
" Shortcut for [] {{{

onoremap ir i[
onoremap ar a[
vnoremap ir i[
vnoremap ar a[

" }}} Shortcut for []
" Numbers {{{

" Motion for numbers
" onoremap N :<C-u>call <SID>NumberTextObject(0)<CR>
" xnoremap N :<C-u>call <SID>NumberTextObject(0)<CR>
" onoremap aN :<C-u>call <SID>NumberTextObject(1)<CR>
" xnoremap aN :<C-u>call <SID>NumberTextObject(1)<CR>
" onoremap iN :<C-u>call <SID>NumberTextObject(1)<CR>
" xnoremap iN :<C-u>call <SID>NumberTextObject(1)<CR>

" function! s:NumberTextObject(whole)
"     let num = '\v[0-9]'

"     " If the current char isn't a number, walk forward.
"     while getline('.')[col('.') - 1] !~# num
"         normal! l
"     endwhile

"     " Now that we're on a number, start selecting it.
"     normal! v

"     " If the char after the cursor is a number, select it.
"     while getline('.')[col('.')] =~# num
"         normal! l
"     endwhile

"     " If we want the entire word, flip the select point and walk
"     if a:whole
"         normal! o

"         while col('.') > 1 && getline('.')[col('.') - 2] =~# num
"             normal! h
"         endwhile
"     endif
" endfunction

" }}} Numbers

" }}} Text Objects
" Mini-plugins ------------------------------------------------------------ {{{

" " Ack motions {{{
" TODO: Figure out ack.vim plugin

" " Motions to Ack for things. Works with most motions, including
" "
" "  w, W, e, E, b, B, t*, f*, i*, a*, and custom text objects.
" "
" "  Note: If the text is covered by a motion contain a newline it won't work.
" "  Ack seaches line-by-line.

" nnoremap <silent> <leader>A :set opfunc=<SID>AckMotion<CR>g@
" " xnoremap <silent> <leader>A :<C-U>call <SID><AckMotion(visualmode())<CR>

" nnoremap <leader>A :Ack! '\b<C-r><C-w>\b'<CR>
" nnoremap <bs> :Ack! '\b<C-r><C-w>\b'<CR>
" xnoremap <silent> <bs> :<C-U>call <SID>AckMotion(visualmode())<CR>

" function! s:CopyMotionForType(type)
"     if a:type ==# 'v'
"         silent execute "normal! `<" . a:type . "`>y"
"     elseif a:type ==# 'char'
"         silent execute "normal! `[v`]y"
"     endif
" endfunction

" function! s:AckMotion(type) abort
"     let reg_save = @@

"     call s:CopyMotionForType(a:type)

"     execute "normal! :Ack! --literal " . shellescape(@@) . "\<CR>"

"     let @@ = reg_save
" endfunction

" }}} Ack motions
" Pulse Line {{{

function! s:Pulse() " {{{
    redir => old_hi
        silent execute 'hi CursorLine'
    redir END
    let old_hi = split(old_hi, '\n')[0]
    let old_hi = substitute(old_hi, 'xxx', '', '')

    let steps = 8
    let width = 1
    let start = width
    let end = steps * width
    let color = 233

    for i in range(start, end, width)
        execute "hi CursorLine ctermbg=" . (color + i)
        redraw
        sleep 6m
    endfor
    for i in range(end, start, -1 * width)
        execute "hi CursorLine ctermbg=" . (color + i)
        redraw
        sleep 6m
    endfor

    execute 'hi ' . old_hi
endfunction " }}}
command! -nargs=0 Pulse call s:Pulse()

" }}} Pulse Line
" Highlight Word {{{
"
" This mini-plugin provides a few mappings for highlighting words temporarily.
"
" Sometimes you're looking at a hairy piece of code and would like a certain
" word or two to stand out temporarily. You can search for it, but that only
" gives you one colour of highlighting. Now you can use <leader>N where N is
" a number from 1-6 to highlight the current word in a specifc colour.

function! HiInterestingWord(n) " {{{
    " Save our location.
    normal! mz

    " Yank the current word into the z register.
    normal! "zyiw

    " Calculate an arbitrary match ID. Hopefully nothing else is using it.
    let mid = 86750 + a:n

    " Clear existing matches, but don't worry if they don't exist.
    silent! call matchdelete(mid)

    " Construct a literal pattern that has to match at boundaries.
    let pat = '\V\<' . escape(@z, '\') . '\>'

    " Actually match the words.
    call matchadd("InterestingWord" . a:n, pat, 1, mid)

    " Move back to our original location.
    normal! `z
endfunction " }}}

" Mappings {{{

nnoremap <silent> <leader>1 :call HiInterestingWord(1)<CR>
nnoremap <silent> <leader>2 :call HiInterestingWord(2)<CR>
nnoremap <silent> <leader>3 :call HiInterestingWord(3)<CR>
nnoremap <silent> <leader>4 :call HiInterestingWord(4)<CR>
nnoremap <silent> <leader>5 :call HiInterestingWord(5)<CR>
nnoremap <silent> <leader>6 :call HiInterestingWord(6)<CR>

" }}} Mappings
" Default Highlights {{{

hi def InterestingWord1 guifg=#000000 ctermfg=16 guibg=#ffa724 ctermbg=214
hi def InterestingWord2 guifg=#000000 ctermfg=16 guibg=#aeee00 ctermbg=154
hi def InterestingWord3 guifg=#000000 ctermfg=16 guibg=#8cffba ctermbg=121
hi def InterestingWord4 guifg=#000000 ctermfg=16 guibg=#b88853 ctermbg=137
hi def InterestingWord5 guifg=#000000 ctermfg=16 guibg=#ff9eb8 ctermbg=211
hi def InterestingWord6 guifg=#000000 ctermfg=16 guibg=#ff2c4b ctermbg=195

" }}} Default Highlights

" }}} Highlight Word

" }}} Mini-plugins
" Environments (GUI/Console) ---------------------------------------------- {{{

if has('gui_running')
    " GUI Vim

    " set guifont=

    " Remove all the UI cruft
    set go-=T
    set go-=l
    set go-=L
    set go-=r
    set go-=R

    set guitablabel=%M\ %t

else
    " Console Vim

    " Colour support
    set t_Co=256

    " Mouse support
    set mouse=a
endif

" }}} Environments (GUI/Console)
" Training ---------------------------------------------------------------- {{{

nnoremap <left> <nop>
nnoremap <right> <nop>
nnoremap <up> <nop>
nnoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>

inoremap <esc> <nop>

" }}} Training
" Local ------------------------------------------------------------------- {{{

" Source separate vimrc files with any local-only settings
if filereadable("~/.vimrc.local")
  source ~/.vimrc.local
endif

if filereadable(".vimrc.local")
  source .vimrc.local
endif

" }}} Local

