# .bashrc
# Author: Jonathan Vander Mey <jonathan@vandermey.ca>
# Source: https://gitlab.com/grimmn/dotfiles.git

# Bash configuration 

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# Additional files --------------------------------------------------------
# Local settings
if [ -f ~/.localrc ]; then
    . ~/.localrc
fi

# Alias definitions.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Check for custom user bin directory
if [ -d ~/bin ]; then
    export PATH=~/bin:$PATH
fi

# Check for Snap bin directory
if [ -d /snap/bin ]; then
    export PATH=/snap/bin:$PATH
fi

# Check for Rust install directory
if [ -d ~/.cargo/bin ]; then
    export PATH="$HOME/.cargo/bin:$PATH"
fi

# Add path directory for custom scripts
export PATH=$DOTFILES/bin:$PATH

# General Settings --------------------------------------------------------
if [[ -x /usr/bin/nvim ]]; then
  editor=nvim
elif [[ -x /usr/bin/vim ]]; then
  editor=vim
else
  editor=vi
fi
export EDITOR=$editor
export SVN_EDITOR=$editor
export GIT_EDITOR=$editor
export ALIAS=$USER

# Window & Display --------------------------------------------------------

export TERM="xterm-256color"

force_color_prompt=yes

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac


if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

# NOTE: 2/28/17 Removed for the moment because of long prompt load times on
# large svn repos
# function _update_ps1() {
#   PS1="$(~/dotfiles/bash/powerline-shell/powerline-shell.py $? 2> /dev/null)"
# }

if [ "$color_prompt" = yes ]; then
  PS1="┌─[\`if [ \$? = 0 ]; then echo \[\e[32m\]✔\[\e[0m\]; else echo \[\e[31m\]✘\[\e[0m\]; fi\`]───[\[\e[01;49;39m\]\u\[\e[00m\]\[\e[01;49;39m\]@\H\[\e[00m\]]───[\[\e[1;49;34m\]\w\[\e[0m\]]───[\[\e[1;49;39m\]\$(ls | wc -l) files, \$(ls -lah | grep -m 1 total | sed 's/total //')\[\e[0m\]]\n└───▶ "
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# if [ "$TERM" != "linux" ]; then
#   PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
# fi

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# History options ---------------------------------------------------------

# don't put duplicate lines or lines starting with space in the history.
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000


# Completion --------------------------------------------------------------
# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Ansible completion
_have() { type "$1" &>/dev/null; }
_have ansible && . <(register-python-argcomplete ansible)
_have ansible-config && . <(register-python-argcomplete ansible-config)
_have ansible-console && . <(register-python-argcomplete ansible-console)
_have ansible-doc && . <(register-python-argcomplete ansible-doc)
_have ansible-galaxy && . <(register-python-argcomplete ansible-galaxy)
_have ansible-inventory && . <(register-python-argcomplete ansible-inventory)
_have ansible-playbook && . <(register-python-argcomplete ansible-playbook)
_have ansible-pull && . <(register-python-argcomplete ansible-pull)
_have ansible-vault && . <(register-python-argcomplete ansible-vault)

# Fix stupid shit ---------------------------------------------------------
# So as not to be disturbed by Ctrl-S and ctrl-Q in terminals:
stty -ixon
